<?php
	defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

	define("ATR_CWD", plugin_dir_path(__FILE__)); 
	define("ATR_URI", plugin_dir_url(__FILE__));
<?php
namespace ATR {
	defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

	class Bogus_Remover extends Singleton {
		public function __construct() {
			$this->version_delimiter = "?ver=";
			$this->delimiter_quoted = preg_quote($this->version_delimiter);

			add_filter( 'script_loader_src', [$this, "replace_version_tag"], 11, 2);
			add_filter( 'style_loader_src', [$this, "replace_version_tag"], 11, 2);

			$this->add_filters( array(
				"json_enabled",
				"json_jsonp_enabled",
				"rest_enabled",
				"rest_jsonp_enabled",
				"xmlrpc_enabled"
			) );

			$this->remove_actions( array(
				"wp_head" => array(
					'feed_links'                      => 2,  // Feed Links
					'feed_links_extra'                => 3,  // Feed Links
					'adjacent_posts_rel_link_wp_head' => 10,
					'print_emoji_detection_script'    => 10,
					'parent_post_rel_link'            => 10,
					'wp_shortlink_wp_head'            => 10,
					'start_post_rel_link'             => 10,
					'wlwmanifest_link'                => 10, // Windows Live Writer
					'rsd_link'                        => 10, // Windows Live Writer
					'index_rel_link'                  => 10,
					'wp_generator'                    => 10, // WP generator tag
				)
			) );
		}

		/**
		* Removes ?ver=X.X from filename and replaces the version number with it's crc32 representation
		* By replacing it we should ensure the representation changes when version changes,
		* Simply because i'm against explicitly exposing any version numbers to potential bots.
		* Sure we could use crypt(); or hash_hmac(); but i'd consider that overkill.
		*/
		public function replace_version_tag($src) {
			preg_match("/".$this->delimiter_quoted."(.*)/", $src, $version);
			if(!empty($version)) {
				return str_replace($version[0], "?crc32=".crc32($version[1]), $src);
			} else {
				return $src;
			}
		}

		/**
		* Removes specified filters using a callback
		*/
		public function add_filters($filters, $callback = "__return_false") {
			foreach($filters as $filter_name) {
				add_filter($filter_name, $callback);
			}
		}

		/**
		* Removes to given actions in a given priority at a given time.
		* Structured as follows:
		* $tag => [$function_to_remove => $priority]
		* @link https://codex.wordpress.org/Function_Reference/remove_action
		*/
		public function remove_actions($actions) {
			foreach($actions as $tag => $action_names) {
				foreach($action_names as $function => $priority) {
					remove_action($tag, $function, $priority);
				}
			}
		}
	}

	Bogus_Remover::get_instance();
}
<?php
	defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
	global $wpdb;
?>
<div id="atr-dashboard">
	<?php if($this->is_default_prefix()): ?>
		<div class="notice notice-error"><p>Default DB prefix is blank or <strong>wp_</strong> you can fix this using the <a href="https://da.wordpress.org/plugins/db-prefix-change/" target="_blank">Change DB Prefix</a> plugin.</p></div>
	<?php else: ?>
		<div class="notice notice-success"><p>Default DB prefix is not blank or <strong>wp_</strong>.</p></div>
	<?php endif; ?>

	<?php if(file_exists(ABSPATH."readme.html")): ?>
		<div class="notice notice-error"><p>The file <strong>readme.html</strong> exists in document root, it should be deleted.</p></div>
	<?php else: ?>
		<div class="notice notice-success"><p>The file <strong>readme.html</strong> does not exist in document root.</p></div>
	<?php endif; ?>

	<?php if(file_exists(ABSPATH."license.txt")): ?>
		<div class="notice notice-error"><p>The file <strong>license.txt</strong> exists in document root, it should be deleted.</p></div>
	<?php else: ?>
		<div class="notice notice-success"><p>The file <strong>license.txt</strong> does not exist in document root.</p></div>
	<?php endif; ?>

	<?php if($this->has_easily_guessable_user()): ?>
		<div class="notice notice-error"><p>A user with the username <strong><?php print $this->has_easily_guessable_user(); ?></strong> exists and is easily guessable.</p></div>
	<?php else: ?>
		<div class="notice notice-success"><p>No users with easily guessable usernames found.</p></div>
	<?php endif; ?>

	<?php if(!defined("DISALLOW_FILE_EDIT") || (defined("DISALLOW_FILE_EDIT") && !DISALLOW_FILE_EDIT) ): ?>
		<div class="notice notice-warning"><p>File editing appears to be allowed from backend, fix this by defining <code>define("DISALLOW_FILE_EDIT", false);</code></p></div>
	<?php else: ?>
		<div class="notice notice-success"><p>Backend file editing has been disabled.</p></div>
	<?php endif; ?>

	<?php if($this->xmlrpc_is_enabled()): ?>
		<div class="notice notice-warning"><p>XMLRPC is enabled, if you do not use this, it should be disabled.</p></div>
	<?php else: ?>
		<div class="notice notice-success"><p>XMLRPC has been disabled.</p></div>
	<?php endif; ?>

	<?php if(defined("WP_DEBUG") && WP_DEBUG): ?>
		<?php if(defined("WP_DEBUG_DISPLAY") && !WP_DEBUG_DISPLAY): ?>
			<div class="notice notice-error"><p>WP_DEBUG is enabled, <code>define("WP_DEBUG", false);</code> to disable.</p></div>
		<?php elseif(!defined("WP_DEBUG_LOG") || (defined("WP_DEBUG_LOG") && !WP_DEBUG_LOG)): ?>
			<div class="notice notice-warning"><p>WP_DEBUG is enabled but WP_DEBUG_DISPLAY is disabled consider using <code>define("WP_DEBUG_LOG", true);</code>.</p></div>
		<?php endif; ?>
	<?php else: ?>
		<div class="notice notice-success"><p>WP_DEBUG is disabled all together.</p></div>
	<?php endif; ?>

	<?php if(defined("AUTOMATIC_UPDATER_DISABLED") && AUTOMATIC_UPDATER_DISABLED || !apply_filters("automatic_updater_disabled", true)): ?>
		<div class="notice notice-error"><p>Automatic updates is disabled, <code>define("AUTOMATIC_UPDATER_DISABLED", false);</code> or <code>add_filter( 'auto_update_core', '__return_true' );</code>  to enable.</p></div>
	<?php else: ?>
		<?php if(defined("WP_AUTO_UPDATE_CORE") && WP_AUTO_UPDATE_CORE || apply_filters("auto_update_core", false)): ?>
			<div class="notice notice-success"><p>Automatic core update is enabled.</p></div>
		<?php else: ?>
			<div class="notice notice-error"><p>Automatic core update is disabled use <code>define("WP_AUTO_UPDATE_CORE", true);</code> or <code>add_filter( 'auto_update_core', '__return_true' );</code>.</p></div>
		<?php endif; ?>

		<?php if(apply_filters( 'auto_update_plugin', false )): ?>
			<div class="notice notice-success"><p>Automatic plugin update is enabled.</p></div>
		<?php else: ?>
			<div class="notice notice-error"><p>Automatic plugin update is disabled <code>add_filter( 'auto_update_plugin', '__return_true' );</code>.</p></div>
		<?php endif; ?>
	<?php endif; ?>
</div>
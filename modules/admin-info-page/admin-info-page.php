<?php
namespace ATR {
	defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

	class Admin_Info_Page extends Singleton {
		public function __construct() {
			add_action( "admin_menu", array( $this, "admin_menu" ), 11);
			add_action( 'admin_enqueue_scripts', array($this, "admin_styles" ) );
		}

		public function admin_menu_content() {
			require dirname( __FILE__ )."/status-report.php";
		}

		public function admin_menu() {
			add_submenu_page( "options-general.php", "ATR Security Status Report", "Status Report", "manage_options", "atr-security-admin.php", array( $this, "admin_menu_content" ) );
		}

		public function admin_styles() {
			$adminstyles = [
				"atr-security" => "atr-security-admin-info-page.css",
			];

			foreach($adminstyles as $handle => $filename) wp_enqueue_style($handle, ATR_URI."modules/admin-info-page/stylesheets/".$filename);
		}

		public function is_default_prefix() {
			global $wpdb;
			return strtolower($wpdb->prefix) == "wp_" || trim($wpdb->prefix) == '';
		}

		public function has_easily_guessable_user() {
			preg_match("/[^.]*/", str_replace("www.", '', $_SERVER["SERVER_NAME"]), $match);

			$users = [
				"admin",
				"administrator",
				$match[0],
				$_SERVER["SERVER_NAME"],
				str_replace('.', '', $_SERVER["SERVER_NAME"]),
			];

			foreach($users as $username) {
				if(username_exists($username)) return $username;
			}

			return false;
		}

		public function xmlrpc_is_enabled() {
			return apply_filters("xmlrpc_enabled", true);
		}
	}

	Admin_Info_Page::get_instance();
}
?>
<?php
namespace ATR {
	defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

	class Redirect_Author_Url extends Singleton {
		public function __construct() {
			add_action( "template_redirect", array( $this, "author_archive_redirect" ) );
		}

		public function author_archive_redirect() {
			if( is_author() ) {
				wp_redirect( home_url(), 301 );
				exit;
			}
		}
	}

	Redirect_Author_Url::get_instance();
}
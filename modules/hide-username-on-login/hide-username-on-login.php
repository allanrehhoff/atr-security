<?php
namespace ATR {
	defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

	class Hide_Username_On_Login extends Singleton {
		public function __construct() {
			add_filter( 'login_errors', array( $this, "new_error_message" ));
		}

		public function new_error_message($message) {
			global $errors;

			return "<strong>Ooops!</strong><br>The credentials you've entered appears invalid.";
			// if (isset($errors->errors['invalid_username']) || isset($errors->errors['incorrect_password'])) {
			// }
			
			// return $message;
		}
	}

	Hide_Username_On_Login::get_instance();
}
?>
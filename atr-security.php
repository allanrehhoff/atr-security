<?php
	/**
	* Plugin Name: ATR Security
	* Plugin URI:  http://sharksmedia.dk
	* Description: Disables rarely used WP core features. Also sntches out commonly overlooked security (by obscurity) measures.
	* Version:     1.0
	* Author:      Allan Thue Rehhoff
	* Author URI:  https://rehhoff.me
	* Text Domain: atr-security
	* License:     GPL-3.0+
	* License URI: http://www.gnu.org/licenses/gpl-3.0.txt
	*/
	defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

	require "constants.php";
	
	// Inittiate modules
	require ATR_CWD."classes/Singleton.php";
	require ATR_CWD."modules/rename-wp-admin/rename-wp-admin.php";
	require ATR_CWD."modules/bogus-remover/bogus-remover.php";
	require ATR_CWD."modules/disable-feeds/disable-feeds.php";
	require ATR_CWD."modules/hide-username-on-login/hide-username-on-login.php";
	require ATR_CWD."modules/redirect-author-url/redirect-author-url.php";
	require ATR_CWD."modules/admin-info-page/admin-info-page.php";
?>
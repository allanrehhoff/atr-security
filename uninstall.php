<?php
	if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
		die( 'No script kiddies please!' );
	}

	global $wpdb;

	if ( is_multisite() ) {
		$blogs = $wpdb->get_col( "SELECT blog_id FROM {$wpdb->blogs}" );

		if ( $blogs ) {
			foreach ( $blogs as $blog ) {
				switch_to_blog( $blog );
				delete_option( 'atr_page' );
			}

			restore_current_blog();
		}

		delete_site_option( 'atr_page' );
	} else {
		delete_option( 'atr_page' );
	}

	delete_option( 'disable_feeds_redirect' );
	delete_option( 'disable_feeds_allow_main' );